import java.time.LocalDate;

public class HygieneRating implements Comparable<HygieneRating> {
	String fhrsid;
	String localAuthorityBusID;
	String busName;
	String busType;
	Integer busTypeID;
	String addressLine1;
	String addressLine2;
	String addressLine3;
	String addressLine4;
	String postcode;
	String ratingValue;
	String ratingKey;
	LocalDate ratingDate;
	Integer laCode;
	String laName;
	String laWebSite;
	String laEmail;
	Integer scoreHygiene;
	Integer scoreStructural;
	Integer scoreConfidence;
	String schemeType;
	Boolean newRatingPending;
	Double longitude;
	Double latitude;
	Boolean ratingNil;

	public static void main(String[] args) {

	}

	public HygieneRating(String fhrsid, String localAuthorityBusID, String busName, String busType, Integer busTypeID,
			String addressLine1, String addressLine2, String addressLine3, String addressLine4, String postcode,
			String ratingValue, String ratingKey, LocalDate ratingDate, Integer laCode, String laName, String laWebSite,
			String laEmail, Integer scoreHygiene, Integer scoreStructural, Integer scoreConfidence, String schemeType,
			Boolean newRatingPending, Double longitude, Double latitude, Boolean ratingNil) {
		this.fhrsid = fhrsid;
		this.localAuthorityBusID = localAuthorityBusID;
		this.busName = busName;
		this.busType = busType;
		this.busTypeID = busTypeID;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.addressLine3 = addressLine3;
		this.addressLine4 = addressLine4;
		this.postcode = postcode;
		this.ratingValue = ratingValue;
		this.ratingKey = ratingKey;
		this.ratingDate = ratingDate;
		this.laCode = laCode;
		this.laName = laName;
		this.laWebSite = laWebSite;
		this.laEmail = laEmail;
		this.scoreHygiene = scoreHygiene;
		this.scoreStructural = scoreStructural;
		this.scoreConfidence = scoreConfidence;
		this.longitude = longitude;
		this.latitude = latitude;
		this.ratingNil = ratingNil;
	}
	
	public HygieneRating(String ratingValue) {
		this.ratingValue = ratingValue;
	}

	public static boolean isSpecVal(HygieneRating hr) {
		boolean special = false;
		if (hr.ratingValue.equals("AwaitingInspection") || hr.ratingValue.equals("Exempt")) {
			special = true;
		}
		return special;
	}

	public static LocalDate changeToDate(String record) {
		if (record == null) {
			return null;
		}
		return LocalDate.parse(record);
	}

	public static Double changeToDouble(String record) {
		if (record == null) {
			return null;
		}
		return Double.parseDouble(record);
	}

	public static Integer changeToInt(String record) {
		if (record == null) {
			return null;
		}
		return Integer.parseInt(record);
	}

	public static Boolean changeToBoolean(String record) {
		if (record == null) {
			return null;
		}
		return Boolean.parseBoolean(record);
	}

	public static boolean matchCriteria(HygieneRating r, int userChoice, int otherUserChoice, int decision) {
		boolean sorted = false;
		switch (decision) {
		case 1:
			if (Integer.parseInt(r.ratingValue) > userChoice) {
				sorted = true;
			}
			break;
		case 2:
			if (Integer.parseInt(r.ratingValue) < userChoice) {
				sorted = true;
			}
			break;
		case 3:
			if (Integer.parseInt(r.ratingValue) >= userChoice && Integer.parseInt(r.ratingValue) <= otherUserChoice) {
				sorted = true;
			}
		}
		return sorted;
	}

	public static void printInOrder(HygieneRating h) {
		System.out.println(h.busName + " | " + h.busType + " | " + h.addressLine1 + " | " + h.addressLine2 + " | "
				+ h.ratingValue + " | " + h.scoreHygiene + " | " + h.scoreStructural + " | " + h.scoreConfidence + " | "
				+ h.ratingDate);
	}

	@Override
	public int compareTo(HygieneRating o) {
		// TODO Auto-generated method stub
		return 0;
	}
}