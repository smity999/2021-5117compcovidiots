
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class BSTDupNode<E extends Comparable<E>> {
	public BSTDupNode<E> parent; // The parent node
	public BSTDupNode<E> left;
	public BSTDupNode<E> right;
	public E element; // the actual data the node stores
	public List<E> elements; // elements in the case of duplicate keys
	private Comparator<E> comp;

	public BSTDupNode(E value) {
		this(value, null);
	}

	public BSTDupNode(E value, Comparator<E> comp) {
		this.element = value;
		this.comp = comp;
	}

	public String stringType() {
		return "(" + super.toString() + "), " + element + " L:" + (left != null) + ", R:" + (right != null);
	}

	// Uses the custom comparator if specified, or the
	// "natural-order" (i.e. .compareTo) if it is null
	private int nodeCompare(E key, BSTDupNode<E> node) {
		int cmp = 0;
		if (comp != null)
			cmp = comp.compare(key, node.element);
		else
			cmp = key.compareTo(node.element);
		return cmp;

	}

	public BSTDupNode<E> search(E key) {
		// Using the comparable functionality to do the comparison
		int cmp = nodeCompare(key, this);
		if (cmp == 0)
			return this;
		else if (cmp < 0 && this.left != null)
			return this.left.search(key);
		else if (cmp > 0 && this.right != null)
			return this.right.search(key);
		return null;
	}

	public BSTDupNode<E> insertdups(E key) {
		return insertdups(this, key);
	}

	public BSTDupNode<E> insertdups(BSTDupNode<E> node, E key) {
		if (node == null)
			return new BSTDupNode<E>(key, comp);

		int cmp = nodeCompare(key, node);

		// allow duplicates - make a list
		if (cmp == 0) {
			if (node.elements == null) {
				node.elements = new ArrayList<>();
			}
			node.elements.add(key);

		} else if (cmp < 0) {
			node.left = insertdups(node.left, key);
			if (node.left != null)
				node.left.parent = node;
		} else if (cmp > 0) {
			node.right = insertdups(node.right, key);
			if (node.right != null)
				node.right.parent = node;

		}
		return node;
	}

	public List<E> inOrderTraverseSearch(List<E> list, E key) {
		if (list == null) {
			list = new ArrayList<E>();
		}

		if (left != null) {
			left.inOrderTraverseSearch(list, key);
		}

		if (this.search(key) == this) {
			list.add(this.element);
			if (this.elements != null) {
				list.addAll(this.elements);
			}
		}

		if (right != null) {
			right.inOrderTraverseSearch(list, key);
		}

		return list;
	}

	public void preOrderTraversePrint() {
		System.out.println(this.stringType());
		if (left != null)
			left.preOrderTraversePrint();
		if (right != null)
			right.preOrderTraversePrint();
	}
}
