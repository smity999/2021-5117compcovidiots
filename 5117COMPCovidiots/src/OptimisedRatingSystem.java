
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;
import java.text.ParseException;
import java.time.LocalDate;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

public class OptimisedRatingSystem {
	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {
		Scanner userInput = new Scanner(System.in);
		ArrayList<HygieneRating> ratings = new ArrayList<>();
		BSTDupNode<HygieneRating> ratingsTree = null;
		ratingsTree = readFile(ratings, ratingsTree);
		boolean finished = false;
		while (!finished) {
			finished = menu(userInput, ratings, finished, ratingsTree);
		}
	}

	/**
	 * Menu system for the user, making it easier for them to use the program.
	 * 
	 * @param userInput : The user's input.
	 * @param ratings   : A list of all of the stored hygiene ratings.
	 * @param finished  : A boolean to check whether the user has finished using the
	 *                  program.
	 * @return : Returns the finished to signify whether the program has finished
	 *         being used.
	 */
	public static boolean menu(Scanner userInput, ArrayList<HygieneRating> ratings, boolean finished,
			BSTDupNode<HygieneRating> ratingsTree) {
		ArrayList<HygieneRating> nullVals = new ArrayList<HygieneRating>();
		HashMap<String, Boolean> results = new HashMap<String, Boolean>();
		boolean validAction = false;
		int action = 0;
		System.out.println("Please enter the action you would like to take: ");
		System.out.println("1. List all Local Authorities");
		System.out.println("2. List all businesses");
		System.out.println("3. List all ratings for chosen business");
		System.out.println("4. List specific rating criteria");
		System.out.println("5. Least or most satifactory ratings");
		System.out.println("6. List best and worst rated businesses in local authority");
		System.out.println("7. Special Feature");
		System.out.println("8. Quit");
		while (!validAction) {
			try {
				action = Integer.parseInt(userInput.nextLine());
			} catch (Exception e) {
			}
			switch (action) {
			case 1:
				validAction = true;
				listAuthorities(ratings, results);
				break;
			case 2:
				validAction = true;
				listBusinesses(ratings, results);
				break;
			case 3:
				validAction = true;
				listBusinessRatings(ratings, userInput, nullVals);
				break;
			case 4:
				validAction = true;
				listSpecificRatings(ratings, userInput, nullVals, ratingsTree);
				break;
			case 5:
				validAction = true;
				satifactoryRatings(ratings, userInput);
				break;
			case 6:
				validAction = true;
				ratingsInArea(ratings, userInput);
				break;
			case 7:
				validAction = true;
				localAreaBusinesses(ratings, userInput, nullVals);
				break;
			case 8:
				validAction = true;
				finished = true;
				break;
			default:
				System.out.println("Please enter a valid action");
			}
			System.out.println();
		}
		return finished;
	}

	/**
	 * Lists all of the local authorities for which the program has access to.
	 * 
	 * @param ratings : A list of all of the stored hygiene ratings.
	 * @param results : A subsection list which will store local authorities which
	 *                have already been listed.
	 */
	public static void listAuthorities(ArrayList<HygieneRating> ratings, HashMap<String, Boolean> results) {
		for (HygieneRating rate : ratings) {
			results.put(rate.laName, true);
		}
		System.out.println("The stored local authorities are: ");
		for (Entry<String, Boolean> r : results.entrySet()) {
			System.out.println(r.getKey());
		}
		System.out.println();
		results.clear();
	}

	/**
	 * @param ratings : A list of all of the stored hygiene ratings.
	 * @param results : A subsection list which will store local authorities which
	 *                have already been listed.
	 */
	public static void listBusinesses(ArrayList<HygieneRating> ratings, HashMap<String, Boolean> results) {
		for (HygieneRating rate : ratings) {
			results.put(rate.busName, true);
		}
		System.out.println("The stored unique businesses are: ");
		for (Entry<String, Boolean> r : results.entrySet()) {
			System.out.println(r.getKey());
		}
		System.out.println();
		System.out.println(results.size());
		results.clear();
	}

	/**
	 * Lists all of the unique local businesses for which the program has access to.
	 * 
	 * @param ratings   : A list of all of the stored hygiene ratings.
	 * @param userInput : The user's input.
	 * @param nullVals  : A list of all the hygiene ratings with null ratingDates.
	 */
	public static void listBusinessRatings(ArrayList<HygieneRating> ratings, Scanner userInput,
			ArrayList<HygieneRating> nullVals) {
		ArrayList<HygieneRating> busResults = new ArrayList<HygieneRating>();
		boolean validBusName = false;
		System.out.println("Please enter the business you would like to see the ratings of: ");
		while (!validBusName) {
			String businessToShow = userInput.nextLine();
			for (HygieneRating rate : ratings) {
				if (smartSearch(rate.busName, businessToShow)) {
					validBusName = true;
					if (rate.ratingDate != null) {
						busResults.add(rate);
					} else {
						nullVals.add(rate);
					}
				}
			}
			if (!validBusName) {
				System.out.println("Please enter a valid business name");
			}
		}
		sortingAlgorithm(busResults, 0, busResults.size() - 1);
		addBackNulls(busResults, nullVals);
		for (HygieneRating h : busResults) {
			HygieneRating.printInOrder(h);
		}
		busResults.clear();
		nullVals.clear();
	}

	/**
	 * When the user enters a business, produces a list of useless information for
	 * businesses which match the search.
	 * 
	 * @param ratings     : A list of all of the stored hygiene ratings.
	 * @param userInput   : The user's input.
	 * @param nullVals    : A list of all of the null values which are collected
	 *                    during the search, which are then appended onto the end of
	 *                    the sorted list of results.
	 * @param ratingsTree : List of all ratings structured into a binary tree
	 */
	public static void listSpecificRatings(ArrayList<HygieneRating> ratings, Scanner userInput,
			ArrayList<HygieneRating> nullVals, BSTDupNode<HygieneRating> ratingsTree) {
		ArrayList<HygieneRating> results = new ArrayList<HygieneRating>();
		boolean validRateChoice = false;
		boolean validNumber = false;
		boolean typeOfBus = false;
		boolean locAuth = false;
		boolean validAuth = false;
		boolean validBus = false;
		int otherUserChoice = 10;
		int userChoice = 10;
		System.out.println("Would you like to see businesses with ratings: ");
		System.out.println("1. Above a specific value:");
		System.out.println("2. Below a specific value:");
		System.out.println("3. In the range of two specific values:");
		System.out.println("4. Containing special ratings");
		while (!validRateChoice) {
			try {
				userChoice = Integer.parseInt(userInput.nextLine());
			} catch (Exception e) {
			}
			switch (userChoice) {
			case 1:
				validRateChoice = true;
				System.out.println("What value would you like to see ratings above?");
				while (!validNumber) {
					try {
						userChoice = Integer.parseInt(userInput.nextLine());
					} catch (Exception e) {
					}
					if (userChoice >= 0 && userChoice <= 5) {
						validNumber = true;
					} else
						System.out.println("Please enter a valid rating");
				}
				for (int i = 5; i > userChoice; i--) {
					String tempString = "" + i;
					HygieneRating greaterThan = new HygieneRating(tempString);
					results.addAll(ratingsTree.search(greaterThan).elements);
					for (HygieneRating r : results) {
						if (r.ratingDate == null) {
							nullVals.add(r);
						}
					}
					results.removeAll(nullVals);
				}
				break;
			case 2:
				validRateChoice = true;
				System.out.println("What value would you like to see ratings below?");
				while (!validNumber) {
					try {
						userChoice = Integer.parseInt(userInput.nextLine());
					} catch (Exception e) {
					}
					if (userChoice >= 0 && userChoice <= 5) {
						validNumber = true;
					} else
						System.out.println("Please enter a valid rating");
				}
				for (int i = 0; i < userChoice; i++) {
					String tempString = "" + i;
					HygieneRating lessThan = new HygieneRating(tempString);
					results.addAll(ratingsTree.search(lessThan).elements);
					for (HygieneRating r : results) {
						if (r.ratingDate == null) {
							nullVals.add(r);
						}
					}
					results.removeAll(nullVals);
				}
				break;
			case 3:
				validRateChoice = true;
				System.out.println("What value would you like to see ratings between? (Lower)");
				while (!validNumber) {
					try {
						userChoice = Integer.parseInt(userInput.nextLine());
					} catch (Exception e) {
					}
					if (userChoice >= 0 && userChoice <= 5) {
						validNumber = true;
					} else
						System.out.println("Please enter a valid rating");
				}
				System.out.println("What value would you like to see ratings between? (Higher)");
				validNumber = false;
				while (!validNumber) {
					try {
						otherUserChoice = Integer.parseInt(userInput.nextLine());
					} catch (Exception e) {
					}
					if (otherUserChoice >= 0 && userChoice <= 5) {
						validNumber = true;
					} else
						System.out.println("Please enter a valid rating");
				}
				for (int i = userChoice; i <= otherUserChoice; i++) {
					String tempString = "" + i;
					HygieneRating between = new HygieneRating(tempString);
					results.addAll(ratingsTree.search(between).elements);
					for (HygieneRating r : results) {
						if (r.ratingDate == null) {
							nullVals.add(r);
						}
					}
					results.removeAll(nullVals);
				}
				break;
			case 4:
				validRateChoice = true;
				HygieneRating awaitInsp = new HygieneRating("AwaitingInspection");
				HygieneRating exempt = new HygieneRating("Exempt");
				results.addAll(ratingsTree.search(awaitInsp).elements);
				results.addAll(ratingsTree.search(exempt).elements);
				for (HygieneRating r : results) {
					if (r.ratingDate == null) {
						nullVals.add(r);
					}
				}
				results.removeAll(nullVals);
				break;
			default:
				System.out.println("Please enter a valid choice for the rating filtering");
				break;
			}
		}
		sortingAlgorithm(results, 0, results.size() - 1);
		addBackNulls(results, nullVals);
		for (HygieneRating hr : results) {
			HygieneRating.printInOrder(hr);
		}
		System.out.println();
		System.out.println("There are " + results.size() + " businesses which match your query");

		boolean noMoreFilter = false;
		while (!noMoreFilter) {
			if (!locAuth || !typeOfBus) {
				boolean validSecChoice = false;
				ArrayList<HygieneRating> filterList = new ArrayList<HygieneRating>();
				System.out.println("Do you want to further filter by:");
				if (!typeOfBus) {
					System.out.println("1. Type Of Business");
				}
				if (!locAuth) {
					System.out.println("2. Local Authority");
				}
				System.out.println("3. No more");
				while (!validSecChoice) {
					try {
						userChoice = Integer.parseInt(userInput.nextLine());
					} catch (Exception e) {
					}

					if (userChoice == 1 || userChoice == 2 || userChoice == 3) {
						validSecChoice = true;
					} else {
						System.out.println("Please enter a valid action");
					}
				}
				switch (userChoice) {
				case 1:
					System.out.println("Please enter a type of business:");
					while (!validBus) {
						String busType = userInput.nextLine();
						for (HygieneRating rate : results) {
							if ((rate.busType.toLowerCase().contains(busType.toLowerCase()))) {
								validBus = true;
								if (rate.ratingValue != null) {
									filterList.add(rate);
								} else {
									nullVals.add(rate);
								}
							}
						}
						if (!validBus) {
							System.out.println("Please enter a valid type of business");
						}

						results.clear();
						results.addAll(filterList);
					}
					typeOfBus = true;
					break;
				case 2:
					System.out.println("Please enter a local authority:");
					while (!validAuth) {
						String authName = userInput.nextLine();
						for (HygieneRating rate : results) {
							if ((rate.laName.toLowerCase().contains(authName.toLowerCase()))) {
								validAuth = true;
								if (rate.ratingDate != null) {
									filterList.add(rate);
								} else {
									nullVals.add(rate);
								}
							}
						}
						if (!validAuth) {
							System.out.println("Please enter a valid local authority");
						}
						results.clear();
						results.addAll(filterList);
					}
					locAuth = true;
					break;
				case 3:
					noMoreFilter = true;
					break;
				default:
					System.out.println("Please enter a valid choice of the the filtering system");
				}
				sortingAlgorithm(results, 0, results.size() - 1);
				addBackNulls(results, nullVals);
				for (HygieneRating hr : results) {
					HygieneRating.printInOrder(hr);
				}
				System.out.println();
				System.out.println("There are " + results.size() + " businesses which match your filtered query");

			} else {
				noMoreFilter = true;
			}
		}
		results.clear();
	}

	/**
	 * Returns a list of hygiene authorities filtered by a set of parameters chosen
	 * and entered by the user. This list can be optionally further filtered by
	 * 
	 * @param ratings   : A list of all of the stored hygiene ratings.
	 * @param userInput : The user's input.
	 */
	public static void satifactoryRatings(ArrayList<HygieneRating> ratings, Scanner userInput) {
		boolean validOrg = false;
		boolean validDecision = false;
		ArrayList<HygieneRating> orgs = new ArrayList<HygieneRating>();
		int satRate = 0;
		ArrayList<HygieneRating> usefulOrganisations = new ArrayList<HygieneRating>();
		String orgName = "";
		String ch = "";
		while (!validOrg) {
			System.out.println("Please enter the organisation you would like to view:");
			orgName = userInput.nextLine();
			for (HygieneRating h : ratings) {
				if (smartSearch(h.busName, orgName)) {
					orgs.add(h);
					validOrg = true;
				}
			}
			if (!validOrg) {
				System.out.println("Please enter a valid organisation");
			}
		}
		if (validOrg) {
			System.out.println("Would you like to view the:");
			System.out.println("1. Most satisfactory ratings");
			System.out.println("2. Least satisfactory ratings");
			boolean allowed = false;
			int satChoice = 0;
			while (!validDecision) {
				try {
					satChoice = Integer.parseInt(userInput.nextLine());
					allowed = true;
				} catch (Exception e) {
				}
				if (allowed) {
					if (satChoice == 1 || satChoice == 2) {
						validDecision = true;
						for (HygieneRating h : orgs) {
							try {
								int numRatVal = Integer.parseInt(h.ratingValue);
								int satScore = ((numRatVal * 5) + (30 - h.scoreHygiene) + (30 - h.scoreConfidence)
										+ (30 - h.scoreStructural));
								if (usefulOrganisations.size() == 0) {
									usefulOrganisations.add(h);
									satRate = satScore;
								} else {
									if (satScore == satRate) {
										usefulOrganisations.add(h);
									} else {
										switch (satChoice) {
										case 1:
											ch = "most";
											if (satScore > satRate) {
												usefulOrganisations.clear();
												usefulOrganisations.add(h);
												satRate = satScore;
											}
											break;
										case 2:
											ch = "least";
											if (satScore < satRate) {
												usefulOrganisations.clear();
												usefulOrganisations.add(h);
												satRate = satScore;
											}
											break;
										}
									}
								}
							} catch (Exception e) {

							}
						}
						System.out.println("The " + ch + " satisfactory rating/s of " + orgName + ":");
						for (HygieneRating h : usefulOrganisations) {
							HygieneRating.printInOrder(h);
						}
					} else {
						System.out.println("Please enter a valid decision");
					}
				}
			}
		}
	}

	/**
	 * For a given organisation, and dependent on the user's choice, finds and
	 * returns the most/least satisfactory rating/s. Each hygiene rating is given
	 * it's own score as a combination of all 4 rating-based factors, which allow
	 * the program to determine the most/least satisfactory.
	 * 
	 * @param ratings   : A list of all of the stored hygiene ratings.
	 * @param userInput : The user's input.
	 */
	public static void ratingsInArea(ArrayList<HygieneRating> ratings, Scanner userInput) {
		int action = 0;
		boolean validAuth = false;
		boolean validAction = false;
		String mostLeast = null;
		String choice = null;
		int count = 0;
		ArrayList<HygieneRating> laResults = new ArrayList<HygieneRating>();
		HashMap<String, Integer> highRatings = new HashMap<String, Integer>();
		System.out.println("Please enter a local authority: ");
		while (!validAuth) {
			String authName = userInput.nextLine();
			for (HygieneRating rate : ratings) {
				if (rate.laName.toLowerCase().startsWith(authName.toLowerCase())) {
					validAuth = true;
					if (rate.ratingDate != null) {
						laResults.add(rate);
					}
				}
			}
			if (!validAuth) {
				System.out.println("Please enter a valid local authority");
			}
		}
		System.out.println("For organisations with:");
		System.out.println("1: The most 0 or 1 ratings");
		System.out.println("2: The most 5 star ratings");
		while (!validAction) {
			try {
				action = Integer.parseInt(userInput.nextLine());
			} catch (Exception e) {
			}
			if (action == 1 || action == 2) {
				validAction = true;
			} else {
				System.out.println("Please enter a valid action");
			}
		}
		switch (action) {
		case 1:
			choice = "most 0 or 1 ratings";
			for (HygieneRating h : laResults) {
				if (h.ratingValue.equals("0") || h.ratingValue.equals("1")) {
					if (!highRatings.containsKey(h.busName)) {
						highRatings.put(h.busName, 1);
					} else {
						count = highRatings.get(h.busName);
						highRatings.put(h.busName, count + 1);
					}
				}
			}
			break;
		case 2:
			choice = "most 5 ratings";
			for (HygieneRating h : laResults) {
				if (h.ratingValue.equals("5")) {
					if (!highRatings.containsKey(h.busName)) {
						highRatings.put(h.busName, 1);
					} else {
						count = highRatings.get(h.busName);
						highRatings.put(h.busName, count + 1);
					}
				}
			}
			break;
		}

		int highestValue = 0;
		for (Entry<String, Integer> k : highRatings.entrySet()) {
			if (k.getValue() > highestValue) {
				highestValue = k.getValue();
				mostLeast = k.getKey();
			}
		}
		System.out.println("The organisation with the " + choice + " is " + mostLeast + " with " + highestValue);
		highRatings.clear();
	}

	/**
	 * Given the user's entered postcode, returns all businesses which share the
	 * same out-code as the user.
	 * 
	 * @param ratings   : A list of all of the stored hygiene ratings.
	 * @param userInput : The user's input.
	 * @param nullVals  : A list of all the hygiene ratings with null ratingDates.
	 */
	public static void localAreaBusinesses(ArrayList<HygieneRating> ratings, Scanner userInput,
			ArrayList<HygieneRating> nullVals) {
		ArrayList<HygieneRating> areaResults = new ArrayList<HygieneRating>();
		boolean validPost = false;
		int outCodeIndex;
		System.out.println("Please enter your postcode: (In format xxx yyy or xx yyy)");
		while (!validPost) {
			String userPost = userInput.nextLine().toUpperCase();

			if (!userPost.contains(" ")) {
				System.out.println("Please enter a valid postcode");
				continue;
			}

			outCodeIndex = userPost.indexOf(" ");
			userPost = userPost.substring(0, Math.min(userPost.length(), outCodeIndex));

			for (HygieneRating hr : ratings) {
				if (hr.postcode != null) {

					if (outCodeIndex == 2) {
						if (hr.postcode.startsWith(userPost) && hr.postcode.charAt(outCodeIndex) == ' ') {
							validPost = addBusinessResults(nullVals, areaResults, validPost, hr);
						}
					} else {
						if (hr.postcode.startsWith(userPost)) {
							validPost = addBusinessResults(nullVals, areaResults, validPost, hr);
						}
					}
				}
			}
			if (!validPost) {
				System.out.println("Please enter a valid postcode");
			}
		}
		sortingAlgorithm(areaResults, 0, areaResults.size() - 1);
		addBackNulls(areaResults, nullVals);
		for (HygieneRating h : areaResults) {
			HygieneRating.printInOrder(h);
		}
		System.out.println();
		System.out.println("There are " + areaResults.size() + " businesses in your area");
		nullVals.clear();
	}

	/**
	 * Adds elements to nullVals if the ratingDate is null or adds elements to
	 * areaResults if ratingDate is not null
	 * 
	 * @param nullVals    : A list of all of the null values in the user's area
	 *                    which are collected during the search, they are then
	 *                    appended onto the end of the sorted list of results.
	 * @param areaResults : List of all businesses in user's area.
	 * @param validPost   : Checks if user's entered postcode is valid.
	 * @param hr          : One element from the HygieneRating list of elements.
	 * @return : returns the validPost (true or false).
	 */
	public static boolean addBusinessResults(ArrayList<HygieneRating> nullVals, ArrayList<HygieneRating> areaResults,
			boolean validPost, HygieneRating hr) {
		if (hr.ratingDate != null) {
			areaResults.add(hr);
			validPost = true;
		} else {
			nullVals.add(hr);
		}
		return validPost;
	}

	/**
	 * Reads the given CSV files of all of the local authority's hygiene ratings,
	 * and stores them in rating objects. These are then all stored in an array
	 * list. Also makes a binary search tree out of the list for future use.
	 * 
	 * @param ratings     : A list of all of the stored hygiene ratings.
	 * @param ratingsTree : List of all ratings structured into a binary tree.
	 * @return : returns the ratingsTree array.
	 * @throws FileNotFoundException : Exception thrown if any of the files used to
	 *                               create the objects cannot be found.
	 * @throws IOException           : Exception thrown if an input or output error
	 *                               occurs.
	 * @throws ParseException        : Exception thrown if there is an error during
	 *                               the parsing.
	 */
	public static BSTDupNode<HygieneRating> readFile(ArrayList<HygieneRating> ratings,
			BSTDupNode<HygieneRating> ratingsTree) throws FileNotFoundException, IOException, ParseException {
		Reader fileRead = new FileReader("references.txt");
		Scanner read = new Scanner(fileRead);
		while (read.hasNext()) {
			String fileName = ("CSVFiles/" + read.nextLine() + ".csv");
			Reader in = new FileReader(fileName);
			Iterable<CSVRecord> records = CSVFormat.DEFAULT.withHeader().withNullString("").parse(in);
			for (CSVRecord record : records) {
				String fhrsid = record.get("FHRSID");
				String localAuthorityBusID = record.get("LocalAuthorityBusinessID");
				String busName = record.get("BusinessName");
				String busType = record.get("BusinessType");
				Integer busTypeID = HygieneRating.changeToInt(record.get("BusinessTypeID"));
				String addressLine1 = record.get("AddressLine1");
				String addressLine2 = record.get("AddressLine2");
				String addressLine3 = record.get("AddressLine3");
				String addressLine4 = record.get("AddressLine4");
				String postcode = record.get("PostCode");
				String ratingValue = record.get("RatingValue");
				String ratingKey = record.get("RatingKey");
				LocalDate ratingDate = HygieneRating.changeToDate(record.get("RatingDate"));
				Integer laCode = HygieneRating.changeToInt(record.get("LocalAuthorityCode"));
				String laName = record.get("LocalAuthorityName");
				String laWebSite = record.get("LocalAuthorityWebSite");
				String laEmail = record.get("LocalAuthorityEmailAddress");
				Integer scoreHygiene = HygieneRating.changeToInt(record.get("Scores/Hygiene"));
				Integer scoreStructural = HygieneRating.changeToInt(record.get("Scores/Structural"));
				Integer scoreConfidence = HygieneRating.changeToInt(record.get("Scores/ConfidenceInManagement"));
				String schemeType = record.get("SchemeType");
				Boolean newRatingPending = HygieneRating.changeToBoolean(record.get("NewRatingPending"));
				Double longitude = HygieneRating.changeToDouble(record.get("Geocode/Longitude"));
				Double latitude = HygieneRating.changeToDouble(record.get("Geocode/Latitude"));
				Boolean ratingNil = HygieneRating.changeToBoolean(record.get("RatingDate/_xsi:nil"));
				HygieneRating hygiene = new HygieneRating(fhrsid, localAuthorityBusID, busName, busType, busTypeID,
						addressLine1, addressLine2, addressLine3, addressLine4, postcode, ratingValue, ratingKey,
						ratingDate, laCode, laName, laWebSite, laEmail, scoreHygiene, scoreStructural, scoreConfidence,
						schemeType, newRatingPending, longitude, latitude, ratingNil);
				ratings.add(hygiene);
			}
		}
		read.close();
		ratingsTree = convertToBst(ratings, ratingsTree);
		return ratingsTree;
	}

	/**
	 * Converts the arrayList HygieneRating into a Binary Tree list of HygieneRating
	 * 
	 * @param ratings     : A list of all of the stored hygiene ratings.
	 * @param ratingsTree : List of all ratings structured into a binary tree.
	 * @return : returns the ratingsTree array.
	 */
	public static BSTDupNode<HygieneRating> convertToBst(ArrayList<HygieneRating> ratings,
			BSTDupNode<HygieneRating> ratingsTree) {
		Comparator<HygieneRating> sort = new Comparator<HygieneRating>() {
			public int compare(HygieneRating o1, HygieneRating o2) {
				return o1.ratingValue.compareTo(o2.ratingValue);
			}
		};
		ratingsTree = new BSTDupNode<HygieneRating>(ratings.get(0), sort);
		for (int i = 1; i < ratings.size(); i++) {
			ratingsTree.insertdups(ratings.get(i));
		}
		return ratingsTree;
	}

	/**
	 * Quick Sort to sort the ratings from highest to lowest.
	 * 
	 * @param ratings : A list of all of the stored hygiene ratings.
	 * @param low     : The first element in the partition.
	 * @param high    : The last element in the partition.
	 */
	public static void sortingAlgorithm(ArrayList<HygieneRating> ratings, int low, int high) {
		if (low < high) {
			int pivot = partition(ratings, low, high);
			sortingAlgorithm(ratings, low, pivot - 1);
			sortingAlgorithm(ratings, pivot + 1, high);
		}
	}

	/**
	 * Reorder the array so that all elements with values less than the pivot come
	 * before the pivot, while all elements with values greater than the pivot come
	 * after it.
	 * 
	 * @param ratings : A list of all of the stored hygiene ratings.
	 * @param low     : The first element in the partition.
	 * @param high    : The last element in the partition.
	 * @return : Returns pivot point for the sorting algorithm
	 */
	public static int partition(ArrayList<HygieneRating> ratings, int low, int high) {
//		ArrayList<HygieneRating> nullVals = new ArrayList<HygieneRating>();
		HygieneRating pivot = ratings.get(high);
		int i = (low - 1);

		for (int j = low; j < high; j++) {
			// If current element is smaller than the pivot
			if (ratings.get(j).ratingDate.compareTo(pivot.ratingDate) > 0) {
				i++;
				// swap arr[i] and arr[j]
				HygieneRating temp = ratings.get(i);
				ratings.set(i, (ratings.get(j)));
				ratings.set(j, temp);
			}
		}

		// swap arr[i+1] and arr[high] (or pivot)
		HygieneRating temp = ratings.get(i + 1);
		ratings.set(i + 1, ratings.get(high));
		ratings.set(high, (temp));

		return i + 1;
	}

	/**
	 * Called when searching for a specific organisation. Searches whether the name
	 * of the user's inputted organisation is contained in the current organisation
	 * name of the current element in the HygieneRating List
	 * 
	 * @param orgName    : Stores the name of the organisation in the current
	 *                   element in HygieneRating List.
	 * @param orgToCheck : Stores the name of the organisation which is being
	 *                   searched for.
	 * @return : returns fits which determines if the orgToCheck matches the
	 *         orgName.
	 */
	public static boolean smartSearch(String orgName, String orgToCheck) {
		boolean fits = false;
		String orgWithSpace = (" " + orgToCheck + " ");
		if (orgName.toLowerCase().startsWith(orgToCheck.toLowerCase())
				|| orgName.toLowerCase().contains(orgWithSpace.toLowerCase())) {
			fits = true;
		}
		return fits;
	}

	/**
	 * Adds the HygieneRating elements in HygieneRating that have a null ratingDate
	 * to the end of the results list.
	 * 
	 * @param results  : List of businesses in the users area.
	 * @param nullVals : List of businesses in the user's area that have a null
	 *                 ratingDate.
	 */
	public static void addBackNulls(ArrayList<HygieneRating> results, ArrayList<HygieneRating> nullVals) {
		for (HygieneRating hr : nullVals) {
			results.add(hr);
		}
	}

}